测试 druid 和 mybatis 的查询超时

数据库提前 设置了

```
mysql> set autocommit=0;
Query OK, 0 rows affected (0.00 sec)

mysql> select * from tb_user where id = 1 for update;
+----+-----------+-----+
| id | user_name | age |
+----+-----------+-----+
|  1 | xuwei     |  18 |
+----+-----------+-----+
1 row in set (5.49 sec)

mysql> commit;
Query OK, 0 rows affected (0.00 sec)
```

运行测试的例子 UserServiceTest#testGetForUpdate()


当然也可以 用sleep测试