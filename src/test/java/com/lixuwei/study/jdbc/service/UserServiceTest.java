package com.lixuwei.study.jdbc.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class) //使用junit4进行测试
@ContextConfiguration(locations = {"classpath:applicationContext.xml"}) //加载配置文件
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void testGet() {
        userService.getById(1L);
    }

    @Test
    public void testGetForUpdate() {
        long start = System.currentTimeMillis();
        try {

            userService.getByIdForUpdate(1L);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.out.println( "执行时间 " + (System.currentTimeMillis() - start) + "毫秒");
        }
    }
}
