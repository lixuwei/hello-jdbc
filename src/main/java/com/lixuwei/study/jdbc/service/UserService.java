package com.lixuwei.study.jdbc.service;

import com.lixuwei.study.jdbc.entity.User;
import com.lixuwei.study.jdbc.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public void getById(Long id) {
        User byId = userMapper.getById(id);
        System.out.println(byId);
    }

    @Transactional(isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class)
    public void getByIdForUpdate(Long id) {
        User byIdForUpdate = userMapper.getByIdForUpdate(id);
        System.out.println(byIdForUpdate);
    }
}