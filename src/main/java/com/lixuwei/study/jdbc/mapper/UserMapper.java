package com.lixuwei.study.jdbc.mapper;

import com.lixuwei.study.jdbc.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {

    /**
     * query by id
     *
     * @param id
     */
    User getById(Long id);

    /**
     * 查询加锁
     * @param id
     */
    User getByIdForUpdate(Long id);

}